############################### VPC ###############################

resource "aws_vpc" "main" {
  cidr_block           = "${var.cidr}"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = "${merge(
      var.tags,
      map(
        "Name", "Hosting-${var.clint_name}",
      )
    )}"
}

resource "aws_subnet" "subnet-A" {
  vpc_id                  = "${aws_vpc.main.id}"
  cidr_block              = "${var.subnet-A}"
  availability_zone       = "us-west-2a"
  map_public_ip_on_launch = true

  tags = "${merge(
      var.tags,
      map(
        "Name", "${var.clint_name}-subnet-A",
      )
    )}"
}

resource "aws_subnet" "subnet-B" {
  vpc_id                  = "${aws_vpc.main.id}"
  cidr_block              = "${var.subnet-B}"
  availability_zone       = "us-west-2b"
  map_public_ip_on_launch = true

  tags = "${merge(
      var.tags,
      map(
        "Name", "${var.clint_name}-subnet-B",
      )
    )}"
}

resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.main.id}"

  tags = "${merge(
      var.tags,
      map(
        "Name", "${var.clint_name}-IGW-1",
      )
    )}"
}

resource "aws_route" "internet_access" {
  route_table_id         = "${aws_vpc.main.main_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.gw.id}"
}

############################### VPC Peering ###############################

resource "aws_vpc_peering_connection" "auth" {
  peer_vpc_id   = "removed"
  peer_owner_id = "removed"
  peer_region   = "us-west-2"
  vpc_id        = "${aws_vpc.main.id}"

  tags = "${merge(
       var.tags,
      map(
        "Name", "${var.clint_name}-cPrime-Peering-1",
      )
    )}"
}

resource "aws_route" "auth-01" {
  route_table_id            = "${aws_vpc.main.main_route_table_id}"
  destination_cidr_block    = "removed"
  vpc_peering_connection_id = "${aws_vpc_peering_connection.auth.id}"
}

resource "aws_route" "auth-02" {
  route_table_id            = "${aws_vpc.main.main_route_table_id}"
  destination_cidr_block    = "removed"
  vpc_peering_connection_id = "${aws_vpc_peering_connection.auth.id}"
}

resource "aws_route" "monitor-01" {
  route_table_id            = "${aws_vpc.main.main_route_table_id}"
  destination_cidr_block    = "removed"
  vpc_peering_connection_id = "${aws_vpc_peering_connection.auth.id}"
}
