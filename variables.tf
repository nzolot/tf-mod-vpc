variable "clint_name" {}
variable "cidr" {}
variable "subnet-A" {}
variable "subnet-B" {}

variable "tags" {
  type    = "map"
  default = {}
}
